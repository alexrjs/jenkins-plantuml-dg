package org.jenkinsci.plugins.plantuml_dg;

import hudson.Extension;
import hudson.model.AbstractProject;
import hudson.model.Action;
import hudson.model.TransientProjectActionFactory;
import hudson.model.AbstractModelObject;

import java.util.Collection;
import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: alexr_000
 * Date: 15.06.13
 * Time: 11:59
 * To change this template use File | Settings | File Templates.
 */
@Extension
public class PlantUmlProjectActionFactory extends TransientProjectActionFactory {

    @Override
    public Collection<? extends Action> createFor(AbstractProject target) {
        return Collections.<Action>singletonList(new PlantUmlProjectAction(target));
    }

    private AbstractProject url;

    public PlantUmlProjectActionFactory() {

    }

    public PlantUmlProjectActionFactory(AbstractProject url) {
        this.url = url;
    }

}
