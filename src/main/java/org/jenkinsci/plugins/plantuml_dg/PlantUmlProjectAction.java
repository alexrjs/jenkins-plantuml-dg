package org.jenkinsci.plugins.plantuml_dg;

/**
 * Created with IntelliJ IDEA.
 * User: alexr_000
 * Date: 15.06.13
 * Time: 21:34
 * To change this template use File | Settings | File Templates.
 */

import hudson.model.AbstractProject;
import hudson.model.Action;

/**
 * Shows the connected component of the project
 */
public class PlantUmlProjectAction implements Action {

    //final private AbstractProject<?, ?> project;

    public PlantUmlProjectAction()
    {
        //project = null;
    }

    public PlantUmlProjectAction(AbstractProject<?, ?> project) {
        //this.project = project;
    }

    public String getTitle() {
        return "Hello Alex";
    }

    public String getIconFileName() {
        return "/plugin/plantuml-dg/images/plantuml-dg.png";
    }

    public String getDisplayName() {
        return "PlantUML-DG";
    }

    public String getUrlName() {
        return "plantuml_dg";
    }

    public String getMyString() {
        return "Hello Jenkins!";
    }
}
