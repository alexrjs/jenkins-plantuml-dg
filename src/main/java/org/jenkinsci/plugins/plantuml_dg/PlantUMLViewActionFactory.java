package org.jenkinsci.plugins.plantuml_dg;

import hudson.Extension;
import hudson.model.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alexr_000
 * Date: 15.06.13
 * Time: 15:41
 * To change this template use File | Settings | File Templates.
 */

/**
 * Factory to a dependency graph view action to all views
 */
@Extension
public class PlantUMLViewActionFactory extends TransientViewActionFactory {
    /**
     * Shows the connected components containing the projects of the view
     */
    public static class PlantUMLViewAction implements Action {

        private View view;

        public PlantUMLViewAction(View view) {
            this.view = view;
        }

        public String getTitle() {
            return "alex";
        }

        public String getIconFileName() {
/*
            if (this.project == null) {
                return null;
            }
*/
            return "/plugin/plantuml-dg/images/plantuml-dg.png";
        }

        public String getDisplayName() {
/*
            if (this.project == null) {
                return null;
            }
*/
            return "PlantUML-DG";
        }

        public String getUrlName() {
 /*           if (this.project == null) {
                return null;
            }
*/            return "plantuml_dg";
        }
    }

    @Override
    public List<Action> createFor(View v) {
        return Collections.<Action>singletonList(new PlantUMLViewAction(v));
    }

}